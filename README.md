# Holiday 2017

Internal project to review the year's happenings.

* HTML
* Bootstrap and custom CSS
* Javascript & jQuery
* [Live preview](https://camcreates.github.io/projectexamples/projects/holiday)